# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-28 00:19+0000\n"
"PO-Revision-Date: 2022-01-11 20:05+0100\n"
"Last-Translator: mkkDr2010 <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Минчо Кондарев"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mkondarev@yahoo.de"

#: hotspotsettings.cpp:18
#, kde-format
msgid "Hotspot"
msgstr "Активна точка"

#: hotspotsettings.cpp:19
#, kde-format
msgid "Tobias Fella"
msgstr "Tobias Fella"

#: package/contents/ui/main.qml:23
#, kde-format
msgid "Enabled:"
msgstr "Активирано:"

#: package/contents/ui/main.qml:35
#, kde-format
msgid "SSID:"
msgstr "SSID:"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Password:"
msgstr "Парола:"

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Save"
msgstr "Запазване"
