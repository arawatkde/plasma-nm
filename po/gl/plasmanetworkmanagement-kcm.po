# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-13 00:58+0000\n"
"PO-Revision-Date: 2019-01-15 20:21+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kcm.cpp:344
#, kde-format
msgid "my_shared_connection"
msgstr "conexion_compartida"

#: kcm.cpp:416
#, kde-format
msgid "Export VPN Connection"
msgstr "Exportar a conexión de VPN"

#: kcm.cpp:441
#, kde-format
msgid "Do you want to save changes made to the connection '%1'?"
msgstr "Quere gardar os cambios da conexión «%1»?"

#: kcm.cpp:442
#, kde-format
msgctxt "@title:window"
msgid "Save Changes"
msgstr "Gardar os cambios"

#: kcm.cpp:531
#, kde-format
msgid "Import VPN Connection"
msgstr "Importar unha conexión de VPN"

#: kcm.cpp:531
#, fuzzy, kde-format
#| msgid "Export VPN Connection"
msgid "VPN connections (%1)"
msgstr "Exportar a conexión de VPN"

#: qml/AddConnectionDialog.qml:15
msgctxt "@title:window"
msgid "Choose a Connection Type"
msgstr "Escolla un tipo de conexión"

#: qml/AddConnectionDialog.qml:159
msgid "Create"
msgstr "Crear"

#: qml/AddConnectionDialog.qml:169 qml/ConfigurationDialog.qml:110
msgid "Cancel"
msgstr "Cancelar"

#: qml/AddConnectionDialog.qml:186 qml/main.qml:193
msgid "Configuration"
msgstr ""

#: qml/ConfigurationDialog.qml:16
msgctxt "@title:window"
msgid "Configuration"
msgstr ""

#: qml/ConfigurationDialog.qml:37
msgid "General"
msgstr ""

#: qml/ConfigurationDialog.qml:42
msgid "Ask for PIN on modem detection"
msgstr ""

#: qml/ConfigurationDialog.qml:49
#, fuzzy
#| msgid "Export selected connection"
msgid "Show virtual connections"
msgstr "Exportar a conexión seleccionada"

#: qml/ConfigurationDialog.qml:57
msgid "Hotspot"
msgstr ""

#: qml/ConfigurationDialog.qml:63
msgid "Hotspot name:"
msgstr ""

#: qml/ConfigurationDialog.qml:73
msgid "Hotspot password:"
msgstr ""

#: qml/ConfigurationDialog.qml:101
msgid "Ok"
msgstr ""

#: qml/ConnectionItem.qml:99
msgid "Connect"
msgstr "Conectar"

#: qml/ConnectionItem.qml:99
msgid "Disconnect"
msgstr "Desconectar"

#: qml/ConnectionItem.qml:112
msgid "Delete"
msgstr "Eliminar"

#: qml/ConnectionItem.qml:122
msgid "Export"
msgstr "Exportar"

#: qml/ConnectionItem.qml:147
msgid "Connected"
msgstr "Conectado"

#: qml/ConnectionItem.qml:149
msgid "Connecting"
msgstr "Conectando"

#: qml/main.qml:139
msgid "Add new connection"
msgstr "Engadir unha nova conexión"

#: qml/main.qml:153
msgid "Remove selected connection"
msgstr "Retirar a conexión seleccionada"

#: qml/main.qml:169
msgid "Export selected connection"
msgstr "Exportar a conexión seleccionada"

#: qml/main.qml:221
msgctxt "@title:window"
msgid "Remove Connection"
msgstr "Eliminar a conexión"

#: qml/main.qml:222
msgid "Do you want to remove the connection '%1'?"
msgstr "Quere eliminar a conexión «%1»?"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "Buscar…"

#~ msgid "Type here to search connections..."
#~ msgstr "Escriba aquí para buscar conexións…"

#~ msgid "Close"
#~ msgstr "Pechar"
